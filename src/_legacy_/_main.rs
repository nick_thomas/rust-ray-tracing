mod camera;
mod color;
mod hittable;
mod hittable_list;
mod ray;
mod sphere;
mod vec3;

use camera::Camera;
use color::write_color;
use hittable::Hittable;
use rand::thread_rng;
use ray::Ray;
use std::{io::stdout, sync::Arc};
use vec3::{Color, Point3, Vec3};

use crate::{hittable_list::HittableList, sphere::Sphere};

fn hit_sphere(center: &Point3, radius: f64, ray: &Ray) -> f64 {
    let oc = ray.origin - *center;
    let a = ray.direction.length_squared();
    let half_b = oc.dot(ray.direction);
    let c = oc.length_squared() - radius * radius;
    let discriminant = half_b * half_b - a * c;
    if discriminant < 0.0 {
        -1.0
    } else {
        (-half_b - discriminant.sqrt()) / a
    }
}

fn ray_color(ray: &Ray, world: &impl Hittable, depth: i32) -> Color {
    //    let t =  hit_sphere(&Point3::new(0.0, 0.0, -1.0), 0.5, ray);
    //    if t > 0.0 {
    //        let new_vec = (ray.at(t) - Vec3::new(0.0,0.0,-1.0)).normalize();
    //        return 0.5 * Color::new(new_vec.x + 1.0, new_vec.y+1.0, new_vec.z + 1.0);
    //    }
    if depth <= 0 {
        return Color::default();
    }
    if let Some(hit_record) = world.hit(ray, 0.001, f64::INFINITY) {
        let mut random = thread_rng();
        let target: Point3 =
            hit_record.p + hit_record.normal + Vec3::random_in_unit_sphere(&mut random);
        return 0.5
            * ray_color(
                &Ray::new(hit_record.p, target - hit_record.p),
                world,
                depth - 1,
            );
    }
    let unit_direction = ray.direction.normalize();
    let t = 0.5 * (unit_direction.y + 1.0);
    (1.0 - t) * Color::new(1.0, 1.0, 1.0) * t * Color::new(0.5, 0.7, 1.0)
}
fn main() {
    let aspect_ratio = 16.0 / 9.0;
    let image_width = 400;
    let image_height = image_width / aspect_ratio as i32;
    let samples_per_pixel = 100;
    const MAX_DEPTH: i32 = 50;

    // World
    let mut world = HittableList::new();
    world.add(Sphere::new(Point3::new(0.0, 0.0, -1.0), 0.5));
    world.add(Sphere::new(Point3::new(0.0, -100.5, -1.0), 100.0));

    // Camera
    let lookfrom = Point3::new(13.0, 2.0, 3.0);
    let lookat = Point3::new(0.0, 0.0, 0.0);
    let vup = Vec3::new(0.0, 1.0, 0.0);
    let focus_dist = 10.0;
    let aperture = 0.1;

    let cam: Camera = Camera::new(
        lookfrom,
        lookat,
        vup,
        20.0,
        aspect_ratio,
        aperture,
        focus_dist,
    );

    //Render
    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_length = 1.0;

    let origin = Point3::new(0.0, 0.0, 0.0);
    let horizontal = Vec3::new(viewport_width, 0.0, 0.0);
    let vertical = Vec3::new(0.0, viewport_height, 0.0);
    let lower_left_corner =
        origin - horizontal / 2.0 - vertical / 2.0 - Vec3::new(0.0, 0.0, focal_length as f64);

    println!("P3\n{} {}\n255", image_width, image_height);
    let mut pixel_color_container = Vec::new();
    for j in (0..image_height).rev() {
        eprintln!("Scanlines {} ", j);
        for i in 0..image_width {
            let mut pixel_color = Color::default();
            for s in 0..samples_per_pixel {
                let r = i as f64 / (image_width - 1) as f64;
                let g = j as f64 / (image_height - 1) as f64;
                //            let b = 0.25;
                //
                //            let ir = (255.999 * r) as i32;
                //            let ig = (255.999 * g) as i32;
                //            let ib = (255.999 * b) as i32;
                //
                //            println!("{} {} {}", ir, ig, ig);
                let ray = cam.get_ray(r, g);
                pixel_color += ray_color(&ray, &world, MAX_DEPTH);
                pixel_color_container.push(pixel_color);
            }
        }
    }
    eprintln!("Done");
    for pixel_color in pixel_color_container.into_iter() {
        {
            write_color(&mut stdout().lock(), pixel_color, samples_per_pixel).unwrap_or_else(
                |err| {
                    panic!(
                        "Oops, error {} saving color {} for pixel ",
                        err, pixel_color,
                    );
                },
            );
        }
    }
}
